/** \file MtasProcessor.h
 *
 * Header file for MTAS analysis
 */

#ifndef __MTAS_PROCESSOR_H_
#define __MTAS_PROCESSOR_H_
#include "EventManager.h"
#include "DetectorProcessor.h"

class MtasProcessor : public DetectorProcessor 
{
	public:
		MtasProcessor();
		virtual void DeclarePlots(void);
		virtual bool Process(Event &event);
		virtual bool PreProcess(Event &event); 

};

#endif // __MTAS_POCESSOR_H_
