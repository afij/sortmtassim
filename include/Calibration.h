#ifndef __CALIBRATION_H_
#define __CALIBRATION_H_

#include<iostream>
#include<vector>


//using namespace std;
class Event;

class Calibration
{
	public:
		Calibration(std::string inputFileName);
		Calibration();
		~Calibration(){};
		void SetCalibrationParams();
		void Calibrate(Event* event);
	private:
		//int nrOfCalibrationC;
		int calOrderC;
		float thresholdC;
		std::vector<float> polyCoefC;
		int nrOfBufferChannelsC;
		std::vector<float> aChanC;
		std::vector<float> bChanC;
		
		//int nrOfCalibrationIMO;
		int calOrderIMO;
		float thresholdIMO;
		std::vector<float> polyCoefIMO;
		int nrOfBufferChannelsIMO;
		std::vector<float> aChanIMO;
		std::vector<float> bChanIMO;
		
		std::vector<float> calCorrC;
		std::vector<float> calCorrI;
		std::vector<float> calCorrM;
		std::vector<float> calCorrO;
		
		float CalibrateIMO(float light);
		float CalibrateC(float light);
		float CalibrateI(float light);
		float CalibrateM(float light);
		float CalibrateO(float light);
		float SupercalibrateC(float light);
		float SupercalibrateIMO(float light);
		float CalCorrection(float light, std::vector<float> calCorr);
		void FindSuperCalibrationCoefficients(std::vector<float> *coef,
		float thresh, std::vector<float> *aChan, std::vector<float> *bChan,
		int nrOfBufferChannels, int calOrder);
		

};


#endif
