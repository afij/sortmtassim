#ifndef __MTAS_SILI_PROCESSOR_H_
#define __MTAS_SILI_PROCESSOR_H_

#include "EventManager.h"
#include "DetectorProcessor.h"

class MtasSiliProcessor : public DetectorProcessor
{
	private:
		virtual void DeclarePlots(void);
		
	public:
		MtasSiliProcessor();
		virtual bool Process(Event &event);
		virtual bool PreProcess(Event &event); 
		static double siliThreshold;
};

#endif // __MTAS_SILI_POCESSOR_H_
