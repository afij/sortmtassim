/********************************************************************
* Class calculates Gauss Fold. 
* normalization - old histogram value in "i" bin
* miu = old histogram argument in "i" bin
* sigma - calculate from FWHM: sigma = FWHM/2.35
* FWHM = A*(a + b*E)^c *E 
* where A, a, b, c are an imput parameters (FWHMparameters)
********************************************************************/
#ifndef __GAUSS_FOLD_H_
#define __GAUSS_FOLD_H_
#include <iostream>
#include <vector>
#include <TF1.h>
#include "EventManager.h"

class GaussFold
{
	public:
	GaussFold(double maxEnergy);
	~GaussFold();
	void Fold(Event* event);
	
	private:
	std::vector<TF1*> MakeGausses(std::vector <double> FWHMparam);
	float FoldIMO(float signal);	
	float FoldC(float signal);
	float FoldAny(std::vector <TF1*> gausses, float signal);
    std::vector <double> FWHMparamC;
	std::vector <double> FWHMparamIMO;
	int binningFactor;
	double maxEnergy;
	std::vector<TF1*> gaussForC;
	std::vector<TF1*> gaussForIMO;

};
#endif // __GAUSS_FOLD_H_


