#include<iostream>
//#include <string>
using namespace std;

#ifndef __EXCEPTION_H_
#define __EXCEPTION_H_
class Exception
{
	public:
	Exception(const char* message = 0): m_message(message){}
	const char* GetMessage(){return m_message;}
	private:
	const char* m_message;
};

/*class Exception 
{
	public:
		Exception(const char* msg = 0) : message(msg) {}
		const char*  GetMessage() {return message;}
	//private:
 	  const char* message;
};*/

class InvalidArgumentException: public Exception
{
	public:
	InvalidArgumentException(const char*  msg = 0): Exception(msg){}
};


class WrongCalibrationException: public Exception
{
	public:
	WrongCalibrationException(const char*  msg = 0): Exception(msg){}
};

#endif
