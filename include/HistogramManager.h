/* New version of HistogramManager class.
 * W nowej implementacji klasa HistogramManager ma być Singletonem, 
 * bedącym rejestrem histogramów,
 * które chce stworzyć użytkownik. Histogramy są alokowane jak prywatne pole 
 * klasy w postaci mapy. Klasa posiada jedną mapę dla jedno i dwuwymiarowych 
 * histogramów std::map<int, TH1> histogramsRegister, w której alokowane są 
 * histogramy.
 *
 */

#ifndef __HISTOGRAM_MANAGER_H_
#define __HISTOGRAM_MANAGER_H_
#include <map>
#include <string>
#include <set>
#include "TH1.h"
#include "TObjArray.h"

class HistogramManager
{
	public:
	static HistogramManager *GetInstance()
	{
		if (!s_instance)
			s_instance = new HistogramManager;
		return s_instance;
	}
	void DeclareHistogram1D(int id, int size, std::string title);
	void DeclareHistogram2D(int id, int sizeX, int sizeY, std::string title);
	void plot(int id, float value);
	void plot(int id, float valueX, float valueY);
	void saveAllHistograms(TObjArray *hList, int nrOfEvents);
	void SetBinningFactor(double binningFact);
 	
	private:
	HistogramManager();
	~HistogramManager();
	void CheckIfNewId (int id, std::string title);

	std::map <int, TH1*> histogramsRegister;
	std::set <int> histIds1D;
	std::set <int> histIds2D;
	double binningFactor;
	static HistogramManager *s_instance;
};

#endif


