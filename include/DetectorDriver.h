
#ifndef __DETECTOR_DRIVER_H_
#define __DETECTOR_DRIVER_H_

#include "DetectorProcessor.h"
#include <vector>
#include <string>

class DetectorDriver 
{
	private:
		std::vector <DetectorProcessor*> usedProcessors;
		std::vector <std::string> inputFiles;
		int energySize;
		int siliThreshold;
		int nrOfEvents;
		
		void RegisterProcessors();
		void ScanFile(std::string filename);
	public:
		DetectorDriver(std::vector <std::string> inputFiles, int energySize,
						double binningFact);
		~DetectorDriver();
		void SetSiliThreshold(double siliThreshold);
		void SaveAllHistograms(TObjArray *hList);

};

#endif // __DETECTOR_PROCESSOR_H_
