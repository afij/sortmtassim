
#ifndef __DETECTOR_PROCESSOR_H_
#define __DETECTOR_PROCESSOR_H_

#include "HistogramManager.h"
#include "Correlator.h"
#include "EventManager.h"
#include <vector>

class DetectorProcessor 
{
	protected:
		HistogramManager *histogramManager; 
		Correlator *correlator;
		static int energySize;

	public:
		DetectorProcessor();
		virtual void DeclarePlots(void) = 0;
		virtual bool Process(Event &event) = 0;
		virtual bool PreProcess(Event &event) = 0; 
		static void SetEnergySize(int size); 

};

#endif // __DETECTOR_PROCESSOR_H_
