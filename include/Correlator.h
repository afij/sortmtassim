/* 
 * Ubardzo uproszczona wersja Correlatora
 */

#ifndef __CORRELATOR_H_
#define __CORRELATOR_H_
#include <map>
#include <string>

class Correlator
{
	public:
	static Correlator* GetInstance()
	{
		if (!s_instance)
			s_instance = new Correlator();
		return s_instance;
	}
	void AddPlace(std::string placeName, bool value = true);
	void SetPlaceValue(std::string placeName, bool value);
	bool GetPlaceValue(std::string placeName);
 	
	private:
	Correlator();
	~Correlator();
	bool CheckIfNewName (std::string placeName);
	std::map <std::string, bool> placesRegister;
	static Correlator* s_instance;
};



#endif


