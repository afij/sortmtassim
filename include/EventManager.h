#ifndef __EVENT_MANAGER_H_
#define __EVENT_MANAGER_H_

#include<iostream>
#include<sstream>
#include "Exception.h"
#include "Calibration.h"
#include "GaussFold.h"
#include "TTree.h"
#include "TFile.h"



using namespace std;

class Calibration;
class GaussFold;

struct Event
{
	static const int nrOfSilicons = 14;
	static const int nrOfMtasModules = 19;
	float eventID, totEnDep, enDepInMod[19], totLight, lightInMod[19];
	float betaSignal, enInSili[14];

};

class EventManager
{
	public:
	//Conect tree1 and tree2 with EnDepInModulesPart1 and EnDepInModulesPart2 in root file 
	EventManager(string filename, double maxEnergy);
	~EventManager();
	Event GetEvent(int eventNr); //eventNr - which event would you like to get
	int GetNrOfEvents(){return nrOfentries;}
	
	private:
	void ReadFile(string filename);
	void ConnectVariableWithData();
	Calibration* calibration;
	GaussFold* gaussFold;
	string filename;
	string int2str(int i);
	Event event;
	int nrOfentries;
	TFile *file; 
	TTree *treeEn1;
	TTree *treeEn2;
	TTree *treeLight1;
	TTree *treeLight2;
	TTree *treeSilicon1;
	TTree *treeSilicon2;
};

#endif
