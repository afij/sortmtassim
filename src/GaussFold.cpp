#include "GaussFold.h"
#include "EventManager.h"
#include "Exception.h"
#include <cmath>
#include <TMath.h>


GaussFold::GaussFold(double maxEnergy)
{
    FWHMparamC = { 1., 6.0, 0.0018, -1.33};
    FWHMparamIMO = { 1., 6.5, 0.0018, -1.33};
    binningFactor = 100;
	this->maxEnergy = maxEnergy;
	gaussForC = MakeGausses(FWHMparamC);
	gaussForIMO = MakeGausses(FWHMparamIMO);
}

GaussFold::~GaussFold()
{
	for (unsigned i=0; i!=gaussForC.size(); ++i)
		delete gaussForC.at(i);
	for (unsigned i=0; i!=gaussForIMO.size(); ++i)
		delete gaussForIMO.at(i);		
	gaussForC.clear();
	gaussForIMO.clear();
}

std::vector<TF1*> GaussFold::MakeGausses(std::vector <double> FWHMparam)
{
	std::vector<TF1*> gausses;
	double i = 0;
	while(i*binningFactor < maxEnergy)
	{
		
		double mean = i*binningFactor + binningFactor/2.;
		double FWHM = FWHMparam.at(0)*
         pow((FWHMparam.at(1)+FWHMparam.at(2)*mean),FWHMparam.at(3))*mean;
		double sigma = 0.42661*FWHM;
		double maxWidth = 3.*sigma;
		TF1 *gaussFun = new TF1("gaussFun","TMath::Gaus(x,[0],[1],0)",
		-1.*maxWidth, maxWidth);
		gaussFun->SetParameters(0., sigma);
		gausses.push_back(gaussFun);
		i++;
	}
	return gausses;
}

void GaussFold::Fold(Event* event)
{
	const int nrOfCristals = 19;
	for(int i = 0; i != nrOfCristals; ++i)
		if(i < 1)
			(*event).lightInMod[i] = FoldC( (*event).lightInMod[i]);
		else
			(*event).lightInMod[i] = FoldIMO( (*event).lightInMod[i]);
}


float GaussFold::FoldC(float signal)
{
	return FoldAny(gaussForC, signal);
}

float GaussFold::FoldIMO(float signal)
{
	return FoldAny(gaussForIMO, signal);
}

float GaussFold::FoldAny(std::vector <TF1*> gausses, float signal)
{
	if (signal < 1)
		return signal;
		
	int histNr = (int)signal/binningFactor;
	if(histNr >= gausses.size())
		throw Exception("GaussFold::FoldAny signal > maxGaussEnergy");
	double roundValue = gausses.at(histNr)->GetRandom();
	double newValue = signal+roundValue;
	if(newValue > 0)
		return newValue;
	else
		return 0;
}
