#include "Calibration.h"
#include "EventManager.h"
#include <fstream>
#include <string>
#include <math.h>
using namespace std;

Calibration::Calibration(std::string inputFileName)
{	
	ifstream inputfile;
	inputfile.open(inputFileName.c_str());
	if (inputfile.is_open())
	{
		cout << "read calibration" << endl;
		string temp, line;
		vector<float> tempCal;
		for(int i=0; i<4; ++i)
		{
			inputfile >> temp;
			getline(inputfile, line);
			stringstream ss;
			ss << line;
			while(ss)
			{
				float tempNum;
				ss>>tempNum;
				tempCal.push_back(tempNum);
			}
			tempCal.pop_back();
    
			if(i == 0)
				calCorrC.swap(tempCal);
			else if (i == 1)
				calCorrI.swap(tempCal);
			else if (i== 2)
				calCorrM.swap(tempCal);
			else
				calCorrO.swap(tempCal);
			
		}
	}
	else
	{
		cout << "no calibration correction" << endl;
		vector<float> tempCal = {0., 1.};
		calCorrC.assign(tempCal.begin(), tempCal.end());
		calCorrI.assign(tempCal.begin(), tempCal.end());
		calCorrM.assign(tempCal.begin(), tempCal.end());
		calCorrO.assign(tempCal.begin(), tempCal.end());
	}
	SetCalibrationParams();
}

Calibration::Calibration()
{
	SetCalibrationParams();

}

void Calibration::SetCalibrationParams()
{
		//nrOfCalibrationC = 2;
	calOrderC = 1;
	//vector<float> coefTempC  = { 0., 0.905459610365647, -25.4546722869403, 0.94445585826925};	
	//polyCoefC.insert(polyCoefC.begin(), coefTempC.begin(), coefTempC.end());
	polyCoefC = { 0., 0.905459610365647, -25.4546722869403, 0.94445585826925};
	thresholdC = 653;
	nrOfBufferChannelsC = 40;
	
	
	//nrOfCalibrationIMO = 2;
	calOrderIMO = 1;	
	//vector<float> coefTempIMO  = { 0., 0.913561580749167, -29.4612809504182, 0.944611392539562};
	//polyCoefIMO.insert(polyCoefIMO.begin(), coefTempIMO.begin(), coefTempIMO.end());
	polyCoefIMO = { 0., 0.913561580749167, -29.4612809504182, 0.944611392539562};
	thresholdIMO = 949;
	nrOfBufferChannelsIMO = 40;
}


void Calibration::FindSuperCalibrationCoefficients(vector<float> *coef, float thresh,
vector<float> *aChan, vector<float> *bChan, int nrOfBufferChannels, int calOrder)
{
	//C	
	double aLow = (*coef).at(1);
	double aHigh = (*coef).at(calOrder+2);
	double bLow = (*coef).at(0);
	
	double deltaA = aHigh - aLow;
	double aStep = deltaA/(float)(nrOfBufferChannels + 1);
	
	(*aChan).push_back(aLow);
	(*bChan).push_back(bLow);
	
	float xLow =  thresh - nrOfBufferChannels/2;
	
	for(int i = 0; i < nrOfBufferChannels; ++i)
	{
		(*aChan).push_back(aLow + aStep*(i+1.));
		float channelBorder = xLow + i;
		float previousBChan = (*bChan).back();
		(*bChan).push_back(-aStep*channelBorder + previousBChan);
	}
	
	//for(unsigned int i = 0; i < (*aChan).size(); ++i)
	//{
	//	cout << i << " " << xLow + i << " " << (*aChan).at(i) << " " << (*bChan).at(i) << endl;
	//}
	
}


void Calibration::Calibrate(Event* event)
{
	const int nrOfCristals = 19;
	for(int i = 0; i != nrOfCristals; ++i)
		if(i < 1)
			(*event).lightInMod[i] = CalibrateC( (*event).lightInMod[i]);
		else if(i < 7)
			(*event).lightInMod[i] = CalibrateI( (*event).lightInMod[i]);
		else if(i < 13)
			(*event).lightInMod[i] = CalibrateM( (*event).lightInMod[i]);
		else
			(*event).lightInMod[i] = CalibrateO( (*event).lightInMod[i]);
}

float Calibration::CalibrateC(float light)
{
	float energy = 0;

	if(light < thresholdC - nrOfBufferChannelsC/2)
		for(int order = 0; order <calOrderC+1;  ++order )
			energy +=  polyCoefC.at(order)*pow(light, order);
	else if(light >= thresholdC - nrOfBufferChannelsC/2 &&  light < thresholdC + nrOfBufferChannelsC/2)
		energy = SupercalibrateC (light);
	else
		for(int order = 0; order <calOrderC+1;  ++order )
			energy +=  polyCoefC.at(order + calOrderC +1)*pow(light, order);

	return CalCorrection(energy, calCorrC);
}

float Calibration::CalibrateI(float light)
{
	float energy = CalibrateIMO(light);
	return CalCorrection(energy, calCorrI);
}

float Calibration::CalibrateM(float light)
{
	float energy = CalibrateIMO(light);
	return CalCorrection(energy, calCorrM);
}
float Calibration::CalibrateO(float light)
{
	float energy = CalibrateIMO(light);
	return CalCorrection(energy, calCorrO);
}

float Calibration::SupercalibrateC(float light)
{ 
	if(aChanC.size() == 0)
		FindSuperCalibrationCoefficients(&polyCoefC, thresholdC, &aChanC,
		 &bChanC, nrOfBufferChannelsC, calOrderC);
	
	float xLow =  thresholdC - nrOfBufferChannelsC/2;
	int chan = static_cast<int> (light - xLow) + 1;
    double calVal = aChanC.at(chan) * light + bChanC.at(chan);
    return calVal;
}

float Calibration::CalibrateIMO(float light)
{
	float energy = 0;

	if(light < thresholdIMO - nrOfBufferChannelsIMO/2)
		for(int order = 0; order <calOrderIMO+1;  ++order )
			energy +=  polyCoefIMO.at(order)*pow(light, order);
	else if(light >= thresholdIMO - nrOfBufferChannelsIMO/2 &&  light < thresholdIMO + nrOfBufferChannelsIMO/2)
		energy = SupercalibrateIMO (light);
	else
		for(int order = 0; order <calOrderIMO+1;  ++order )
			energy +=  polyCoefIMO.at(order+calOrderIMO + 1)*pow(light, order);

	return energy;
}

float Calibration::SupercalibrateIMO(float light)
{ 
	if(aChanIMO.size() == 0)
		FindSuperCalibrationCoefficients(&polyCoefIMO, thresholdIMO, &aChanIMO,
		 &bChanIMO, nrOfBufferChannelsIMO, calOrderIMO);
	
	float xLow =  thresholdIMO - nrOfBufferChannelsIMO/2;
	int chan = static_cast<int> (light - xLow) + 1;
    double calVal = aChanIMO.at(chan) * light + bChanIMO.at(chan);
	return calVal;
}


float Calibration::CalCorrection(float energy, std::vector<float> calCorr)
{
	float corrEnergy = 0;
	for(unsigned int i=0; i != calCorr.size(); ++i)
	{
		corrEnergy += calCorr.at(i)*pow(energy, (float)i);
	}
	return corrEnergy;
}
