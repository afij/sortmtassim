#include "EventManager.h"


using namespace std;

EventManager::EventManager(string filename, double maxEnergy)
{
 	this -> ReadFile(filename);
 	this -> ConnectVariableWithData();
 	calibration = new Calibration("cal.txt");
 	gaussFold = new GaussFold(maxEnergy);
}
 
 
EventManager::~EventManager()
{
 	delete file;
 	delete calibration;
 	delete gaussFold;
}
 
 void EventManager::ReadFile(string filename)
{
	file = new TFile(filename.c_str());
	treeEn1 = (TTree*)file->Get("EnDepInModulesPart1");
	treeEn2 = (TTree*)file->Get("EnDepInModulesPart2");
	treeLight1 = (TTree*)file->Get("LightPart1");
	treeLight2 = (TTree*)file->Get("LightPart2");
	treeSilicon1 = (TTree*)file->Get("EnDepInSili1");
	treeSilicon2 = (TTree*)file->Get("EnDepInSili2");
}

void EventManager::ConnectVariableWithData()
{
	treeEn1->SetBranchAddress("eventID", &event.eventID);
	treeEn1->SetBranchAddress("TotalEnDep", &event.totEnDep);
	treeLight1->SetBranchAddress("TotalLight", &event.totLight);
	treeSilicon1->SetBranchAddress("TotalSiliEn", &event.betaSignal);

	string nameEnergy = "enDepInMod";
	string nameLight = "LightInMod";
	string nameSiliSignal = "enSili";
	string number[19];
	for(int k=0; k<19; k++)
		number[k]=int2str(k);
		
	string fullNameEnergy, fullNameLight, fullNameSili;
	
	for(int i=0; i<19; i++)
	{
		fullNameEnergy = nameEnergy+number[i];
		fullNameLight = nameLight+number[i];
		if(i<7)
		{
			treeEn1->SetBranchAddress(fullNameEnergy.c_str(), &event.enDepInMod[i]);
			treeLight1->SetBranchAddress(fullNameLight.c_str(), &event.lightInMod[i]);
		}
		else
		{
			treeEn2->SetBranchAddress(fullNameEnergy.c_str(), &event.enDepInMod[i]);
			treeLight2->SetBranchAddress(fullNameLight.c_str(), &event.lightInMod[i]);
		}
 	}
 	//sili
 		for(int i=0; i<14; i++)
	{
		fullNameSili = nameSiliSignal+number[i];
		if(i<7)
		{
			treeSilicon1->SetBranchAddress(fullNameSili.c_str(), &event.enInSili[i]);
		}
		else
		{
			treeSilicon2->SetBranchAddress(fullNameSili.c_str(), &event.enInSili[i]);
		}
 	}
 	nrOfentries = (int)treeEn1->GetEntries();
}

 
Event EventManager::GetEvent(int eventNr)
{
 if(eventNr>nrOfentries)
 	throw Exception("Too high event number");
 	
 else
 	{
 		treeEn1->GetEntry(eventNr);
 		treeEn2->GetEntry(eventNr);
 		treeLight1->GetEntry(eventNr);
 		treeLight2->GetEntry(eventNr);
 		treeSilicon1->GetEntry(eventNr);
 		treeSilicon2->GetEntry(eventNr); 		
 		calibration->Calibrate(&event);
 		gaussFold->Fold(&event);
		return event;
 	}
}

string EventManager::int2str(int i)
{
    stringstream ss;
    string temp;
    ss << i;
    ss >> temp;
    return temp;
}
