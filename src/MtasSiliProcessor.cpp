/*! \file MtasSiliProcessor.cpp
 *
 * The MTAS processor handles detectors of type mtas  */

#include "MtasSiliProcessor.h"
#include "HistogramManager.h"
#include "EventManager.h"
#include <iostream>
#include <fstream>

 
using namespace std;
	
MtasSiliProcessor::MtasSiliProcessor(): DetectorProcessor()
{
	DeclarePlots();
}

void MtasSiliProcessor::DeclarePlots()
{	
	histogramManager->DeclareHistogram1D(1, 20, "Silicon Multiplicity");
	histogramManager->DeclareHistogram1D(2, 20, "Silicon Position");
	//histogramManager->DeclareHistogram1D(2, 20, "Silicon Position");
}

bool MtasSiliProcessor::PreProcess(Event &event)
{
    float maxBetaEnergy = 0;
    int nrOfSilicons = event.nrOfSilicons;
	
    for (int i = 0; i != nrOfSilicons; ++i)
    {	
        if(event.enInSili[i] > maxBetaEnergy)
			maxBetaEnergy = event.enInSili[i];
    }
    
    if(maxBetaEnergy > siliThreshold)
		correlator->SetPlaceValue("SiliSignal", true);
	else
		correlator->SetPlaceValue("SiliSignal", false);
		
	return true;
}

bool MtasSiliProcessor::Process(Event &event)
{
	int nrOfActiveSilicons = 0;
	int nrOfSilicons = event.nrOfSilicons;

    for (int siliNr = 0; siliNr != nrOfSilicons; ++siliNr)
    {	
        if(event.enInSili[siliNr] > siliThreshold)
        {
			nrOfActiveSilicons++;
			histogramManager->plot(2, siliNr);
		}
    }   
	histogramManager->plot(1, nrOfActiveSilicons);
    
	return true;
}

double MtasSiliProcessor::siliThreshold = 10;
