/*! \file MtasProcessor.cpp
 *
 * The MTAS processor handles detectors of type mtas  */

#include "HistogramManager.h"
#include "EventManager.h"
#include "MtasProcessor.h"

using namespace std;
	
MtasProcessor::MtasProcessor() : DetectorProcessor()
{
	DeclarePlots();
}

void MtasProcessor::DeclarePlots(void)
{
	histogramManager->DeclareHistogram1D(6200, energySize, "Total Mtas");
	histogramManager->DeclareHistogram1D(6210, energySize, "Total Central");
	histogramManager->DeclareHistogram1D(6220, energySize, "Total Inner" );
	histogramManager->DeclareHistogram1D(6230, energySize, "Total Middle");
	histogramManager->DeclareHistogram1D(6240, energySize, "Total Outer" );	

	histogramManager->DeclareHistogram1D(6300, energySize, "Total Mtas, beta gated");
	histogramManager->DeclareHistogram1D(6310, energySize, "Total Central, beta gated");
	histogramManager->DeclareHistogram1D(6320, energySize, "Total Inner, beta gated" );
	histogramManager->DeclareHistogram1D(6330, energySize, "Total Middle, beta gated");
	histogramManager->DeclareHistogram1D(6340, energySize, "Total Outer, beta gated" );
	
	//histogramManager->DeclareHistogram1D(6400, energySize, "Total Mtas, no beta");
	//histogramManager->DeclareHistogram1D(6410, energySize, "Total Central, no beta");
	//histogramManager->DeclareHistogram1D(6420, energySize, "Total Inner, no beta" );
	//histogramManager->DeclareHistogram1D(6430, energySize, "Total Middle, no beta");
	//histogramManager->DeclareHistogram1D(6440, energySize, "Total Outer, no beta" );
	
	//Special TAS spectrum I+M+O
	histogramManager->DeclareHistogram1D(6360, energySize, "Total I+M+O, beta-gated");
	histogramManager->DeclareHistogram1D(6260, energySize, "Total I+M+O, no beta-gated");
	
	//sum histograms
	histogramManager->DeclareHistogram1D(6325, energySize, "Sum Inner, beta-gated");
	histogramManager->DeclareHistogram1D(6225, energySize, "Sum Inner");
	histogramManager->DeclareHistogram1D(6365, energySize, "Sum I+M+O, beta-gated");
	histogramManager->DeclareHistogram1D(6265, energySize, "Sum I+M+O");
    
	//2D spectras
	//histogramManager->DeclareHistogram2D(6250, energySize, energySize, "MTAS vs sum Inner");
	histogramManager->DeclareHistogram2D(6350, energySize, energySize, "MTAS vs sum Inner, beta gated");
}

bool MtasProcessor::PreProcess(Event &event) 
{   
	return true;
}

bool MtasProcessor::Process(Event &event)
{
    bool isBetaSignal = correlator->GetPlaceValue("SiliSignal");   

	//0- all mtas, 1 - Central, 2 - Inner, 3 - Middle, 4 - Outer, 5 - I+M+O
	vector <double> totalMtasEnergy (6,-1);
	// 0-5 Central, 6-11 Inner, 12-17 Middle, 18-23 Outer
	int nrOfMtasModules = event.nrOfMtasModules;
	for(int detNr = 0; detNr != nrOfMtasModules; detNr++)
	{
		double energySignal = event.lightInMod[detNr];
		if(detNr == 0)
		{
			totalMtasEnergy.at(1) += energySignal;
			totalMtasEnergy.at(0) += energySignal;
		}
		else if(detNr < 7) //inner
		{
			totalMtasEnergy.at(2) += energySignal;
			totalMtasEnergy.at(0) += energySignal;
			totalMtasEnergy.at(5) += energySignal;		
		}
		else if(detNr < 13) //middle
		{
			totalMtasEnergy.at(3) += energySignal;
			totalMtasEnergy.at(0) += energySignal;
			totalMtasEnergy.at(5) += energySignal;		
		}
		else //outer
		{
			totalMtasEnergy.at(4) += energySignal;
			totalMtasEnergy.at(0) += energySignal;
			totalMtasEnergy.at(5) += energySignal;		
		}		
	}	


   	//TAS			      
	for(int i=0; i<5; i++)
	{
		histogramManager->plot(6200+i*10, totalMtasEnergy.at(i));
		if(isBetaSignal)
		{
			histogramManager->plot(6300+i*10, totalMtasEnergy.at(i));
		}
	}
        
	if(isBetaSignal)
	{
		histogramManager->plot(6360, totalMtasEnergy.at(5));
	}
	histogramManager->plot(6260, totalMtasEnergy.at(5));//no beta
	
	//SUM
	for(int detNr=0; detNr<6; detNr++)
	{
		histogramManager->plot(6225, event.lightInMod[detNr+1]); //Sum I
		histogramManager->plot(6265, event.lightInMod[detNr+1]);
		histogramManager->plot(6265, event.lightInMod[detNr+7]);			
		histogramManager->plot(6265, event.lightInMod[detNr+13]);
			
		if(isBetaSignal) 
		{
			histogramManager->plot(6325, event.lightInMod[detNr+1]); //Sum I
			histogramManager->plot(6365, event.lightInMod[detNr+1]);
			histogramManager->plot(6365, event.lightInMod[detNr+7]);			
			histogramManager->plot(6365, event.lightInMod[detNr+13]);
		}		
	}
	
	//2D hist
	for(int detNr=0; detNr<6; detNr++)
	{
		//histogramManager->plot(6250, totalMtasEnergy.at(0), event.lightInMod[detNr+1]);
		
		if(isBetaSignal) 
		{
			histogramManager->plot(6350, totalMtasEnergy.at(0), event.lightInMod[detNr+1]);
		}	
	}	   
	return true;
}

