#include "Correlator.h"
#include <Exception.h>
#include <sstream>
#include <cstdlib>

Correlator::Correlator()
{	
}

Correlator::~Correlator()
{
	placesRegister.clear();
}

void Correlator::AddPlace(std::string placeName, bool value)
{
	if (CheckIfNewName (placeName))
	{
		placesRegister.insert (std::pair<std::string, bool>(placeName, value));
	}
	else 
	{
        std::cout << "Error in [Correlator::AddPlace]: Place named '"
				  <<  placeName << "' is already in use"
                  << std::endl;
    }

}

void Correlator::SetPlaceValue(std::string placeName, bool value)
{
	if(CheckIfNewName (placeName))
	{
		AddPlace(placeName, value);
	}
	else
	{
		placesRegister.find(placeName)->second = value;
	}

}

bool Correlator::GetPlaceValue(std::string placeName)
{
	if(CheckIfNewName (placeName))
	{
		ostringstream msg;
		msg << "[Correlator::GetPlaceValue]: Place named '"
			<< placeName << " doesn't exist";
		string str = msg.str();
		//cout << "[Correlator::GetPlaceValue]: Place named '"
		//	<< placeName << " doesn't exist";
		throw InvalidArgumentException(str.c_str());
	}
	else
	{
		return placesRegister.find(placeName)->second;
	}

}

bool Correlator::CheckIfNewName (std::string placeName)
{
	return (placesRegister.count(placeName) == 0);
}


Correlator *Correlator::s_instance = 0;


