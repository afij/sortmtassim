#include "HistogramManager.h"
#include "TH1F.h"
#include "TH2F.h"
#include <Exception.h>
#include <sstream>
#include <cstdlib>

using namespace std;
HistogramManager::HistogramManager()
{
	binningFactor = 1.;
}

HistogramManager::~HistogramManager()
{
	histogramsRegister.clear();
}

void HistogramManager::SetBinningFactor(double binningFact)
{
	binningFactor = binningFact;
}

void HistogramManager::DeclareHistogram1D(int id, int size, string title)
{
	try
	{
		CheckIfNewId (id, title);
		char name[10];
		sprintf(name,"%d",id);
		double xMin = 0.;
		double xMax = static_cast <double> (size);
		TH1* newHist = new TH1F(name, title.c_str(), size, xMin , xMax);
		histogramsRegister.insert ( std::pair<int,TH1*>(id, newHist) );
		histIds1D.insert(id);
	}
	catch (Exception& e) {
        std::cerr << "Error in [HistogramManager::DeclareHistogram1D]: " 
                  << e.GetMessage()  << std::endl;

        throw;
    }

}

void HistogramManager::DeclareHistogram2D
						(int id, int sizeX, int sizeY, string title)
{
	try
	{
		
		CheckIfNewId (id, title);
		char name[10];
		sprintf(name,"%d",id);
		double xMin = 0.;
		double xMax = static_cast <double> (sizeX);
		double yMin = 0.;
		double yMax = static_cast <double> (sizeY);
		TH1* newHist = new TH2F(name, title.c_str(), sizeX/binningFactor,
						xMin , xMax, sizeY/binningFactor, yMin, yMax);
		histogramsRegister.insert ( std::pair<int,TH1*>(id, newHist) );
		histIds2D.insert(id);
	}
	catch (Exception& e) {
        std::cerr << "Error in [HistogramManager::DeclareHistogram2D]: " 
                  << e.GetMessage()  << std::endl;

        throw;
    }

}

void HistogramManager::CheckIfNewId (int id, string title)
{
	if (histogramsRegister.count(id) != 0)
	{
		ostringstream msg;
		msg << "[HistogramManager::CheckArguments]: Histogram titled '"
			<< title << "' requests id " << id << " is already in use";
		string str = msg.str();
		throw InvalidArgumentException(str.c_str());
	}
}

void HistogramManager::plot(int id, float value)
{
	if(histIds1D.count(id) == 0)
	{
		ostringstream msg;
		msg << "[HistogramManager::plot]: Histogram id '"
			<< id << " doesn't exist";
		string str = msg.str();
		throw InvalidArgumentException(str.c_str());
	}
	histogramsRegister.find(id)->second->Fill(value);
}

void HistogramManager::plot(int id, float valueX, float valueY)
{
	if(histIds2D.count(id) == 0)
	{
		ostringstream msg;
		msg << "[HistogramManager::plot]: Histogram id '"
			<< id << " doesn't exist";
		string str = msg.str();
		throw InvalidArgumentException(str.c_str());
	}
	histogramsRegister.find(id)->second->Fill(valueX, valueY);
}

void HistogramManager::saveAllHistograms(TObjArray *hList, int nrOfEvents)
{
	for (std::map<int, TH1*>::iterator it = histogramsRegister.begin();
			it != histogramsRegister.end(); ++it)
	{
		std::cout << "save " <<  it->first << " histogram ";
		std::cout << it->second->GetEntries() << " counts" << std::endl ;
		it->second->Scale(1./(float)nrOfEvents);
		hList -> Add(it->second);
	}	
}

HistogramManager *HistogramManager::s_instance = 0;


