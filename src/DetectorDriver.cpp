#include "DetectorDriver.h"
#include "EventManager.h"
#include "HistogramManager.h"
#include "MtasProcessor.h"
#include "MtasSiliProcessor.h"

DetectorDriver::DetectorDriver(std::vector <std::string> inputFiles, int energySize,
								double binningFact)
{
	this->inputFiles = inputFiles;
	this->energySize = energySize;
	
	HistogramManager* histogramManager = HistogramManager::GetInstance();
	histogramManager->SetBinningFactor(binningFact);
	RegisterProcessors();
	nrOfEvents = 0;
}

DetectorDriver::~DetectorDriver()
{
	inputFiles.clear();
}

void DetectorDriver::RegisterProcessors()
{
	DetectorProcessor::SetEnergySize(energySize);
	usedProcessors.push_back(new MtasProcessor());
	usedProcessors.push_back(new MtasSiliProcessor());	
}

void DetectorDriver::SetSiliThreshold(double siliThreshold)
{
	MtasSiliProcessor::siliThreshold = siliThreshold;
}

void DetectorDriver::ScanFile(std::string filename)
{
	EventManager* eventManager = new EventManager(filename, energySize);
	int nrOfEventsInFile = eventManager->GetNrOfEvents();
	nrOfEvents += nrOfEventsInFile;
	for(int i = 0; i != nrOfEventsInFile; ++i)
	{
		Event event = eventManager->GetEvent(i);
		std::vector <DetectorProcessor*>::iterator it;
		for(it = usedProcessors.begin(); it != usedProcessors.end(); ++it)
			(*it)->PreProcess(event);
		for(it = usedProcessors.begin(); it != usedProcessors.end(); ++it)
			(*it)->Process(event);
		//if (i%1000 == 0)
			//cout << "event " << i << endl;
	}
	delete eventManager;
}

void DetectorDriver::SaveAllHistograms(TObjArray *hList)
{
	std::vector <std::string>::iterator it;
	for(it = inputFiles.begin(); it != inputFiles.end(); ++it)
	{
		ScanFile( *(it) );
	}
	HistogramManager* histogramManager = HistogramManager::GetInstance();
	histogramManager->saveAllHistograms(hList, nrOfEvents);
}
