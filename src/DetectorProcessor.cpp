#include "DetectorProcessor.h"

DetectorProcessor::DetectorProcessor()
{
	histogramManager = HistogramManager::GetInstance();
	correlator = Correlator::GetInstance();
}

void DetectorProcessor::SetEnergySize(int size)
{
	energySize = size;
} 

int DetectorProcessor::energySize = 16384;
