#include "TH1F.h"
#include "TObjArray.h"
#include "Exception.h"
#include "DetectorDriver.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "TApplication.h"

using namespace std;	
int main(int argc, char** argv)
{
	
	ifstream inputfile;
	string inputfilename;
	if(argc==1)
	{
		cout << "Input file name:";
		cin >> inputfilename;
		cout << inputfilename << endl;
	}
	else
	{
		inputfilename = argv[1];
		cout << inputfilename << endl;
	}
	inputfile.open(inputfilename.c_str());
	
	if (inputfile.is_open())
	{
		string filenameOutput,temp;
		std::vector<string> filenameRoot;
		int xMax(0);
		double binningFact;
		double siliThreshold = 0.;
		inputfile >> temp;
		while(inputfile.peek() != '\n')
		{
			inputfile >> temp;
			filenameRoot.push_back(temp);
		}
		inputfile >> temp >> filenameOutput;
		inputfile >> temp >> xMax;
		inputfile >> temp >> binningFact;
		inputfile >> temp >> siliThreshold;
		inputfile.close();

		cout << filenameRoot.at(0) << " " << filenameOutput << " "
			 << xMax << " " << siliThreshold << endl;

		try
		{
			TApplication theApp("App", &argc, argv);	
			DetectorDriver *detectorDriver = 
					new DetectorDriver (filenameRoot, xMax, binningFact);
			detectorDriver->SetSiliThreshold(siliThreshold);
			TObjArray *hList = new TObjArray();
			detectorDriver->SaveAllHistograms(hList);
		
			TFile f(filenameOutput.c_str(),"recreate");
			hList->Write();
			f.Close();
	
			delete detectorDriver;
		}
		catch (Exception& e) {
        std::cerr << "Error in main function: " 
                  << e.GetMessage()  << std::endl;

        throw;
    }
	}
	return 0;
}
