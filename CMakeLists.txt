# $Id: CMakeLists.txt 86065 2014-11-07 08:51:15Z gcosmo $
# usage: cmake /Users/karny/Desktop/MTAS/Analiza/sortCalFold2D 
#----------------------------------------------------------------------------
# Setup the project
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(SortCalFold2D)
# The version number.
set (CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)


#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
#option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
#if(WITH_GEANT4_UIVIS)
#  find_package(Geant4 10.3.0 REQUIRED ui_all vis_all)
#else()
#  find_package(Geant4 10.3.0  REQUIRED)
#endif()
#SET (GEANT4_INSTALL_DATADIR /Users/karny/Geant4-10.3.1-Darwin/share/Geant4-10.3.1/data )


# Find ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED)
include(${ROOT_USE_FILE})
set(ROOT_LIBRARIES -L${ROOT_LIBRARY_DIR} -lCore  -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic)

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
# Setup include directory for this project
#
#include(${Geant4_USE_FILE})
include_directories(${PROJECT_SOURCE_DIR}/include
		    ${ROOT_INCLUDE_DIR}
		    ${PROJECT_BINARY_DIR}
		    )


#----------------------------------------------------------------------------
# Locate sources and headers for this project
# NB: headers are included so they will show up in IDEs
#
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cpp)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.h)

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(SortCalFold2D Sort.cpp ${sources} ${headers})
target_link_libraries(SortCalFold2D 
	${ROOT_LIBRARIES}
	)
#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build B1. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
set(SortCalFold2D_SCRIPTS
  cal.txt
  readme
  sortInput.txt
  sortAllFiles.sh
  )

foreach(_script ${SortCalFold2D_SCRIPTS})
  configure_file(
    ${PROJECT_SOURCE_DIR}/${_script}
    ${PROJECT_BINARY_DIR}/${_script}
    COPYONLY
    )
endforeach()


#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
install(TARGETS SortCalFold2D DESTINATION bin)


